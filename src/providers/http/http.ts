import { Http, Headers } from "@angular/http";
import { Injectable } from '@angular/core';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {
  token: any;


  constructor(
    public http: Http,
  ) {
    console.log('Hello HttpProvider Provider');
  }

  registerUser(details) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");

      this.http.post("https://mpsp-dms.herokuapp.com/api/registeruser", JSON.stringify(details), { headers: headers })
      .subscribe(
        res => {
          let data = res.json();
          console.log(data);
          resolve(data);
        },
        err => {
          reject(err);
        }
        );
    });
  }

  registerDog(details) {
    return new Promise((resolve, reject) => {
console.log(details)
      let data = {
        dog_sex : details.dogGender,
        dog_colour : details.dog_colour
      }

      console.log(data)
     
      this.http
        .post(
          "https://mpsp-dms.herokuapp.com/api/regdog",
          data

        )
        .subscribe(
        res => {
          let data = res.json();
          console.log(data);
          resolve(data);
        },
        err => {
          reject(err);
        }
        );
    });
  }

  getAddress(coords: Coordinates) {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + coords.latitude + ',' + coords.longitude + '&key=AIzaSyByX1jhb5I_ty2yNIkVEjn6KYM9khEILnA');
  }

  loginUser(details) {
    return new Promise((resolve, reject) => {
      console.log(details);
      this.http
        .post("https://mpsp-dms.herokuapp.com/auth/login", details)
        .subscribe(
          res => {
            let data = res.json();
            console.log(data);
            this.token = data
            console.log(this.token);
            console.log(this.token.result);

            if(this.token.result != null){
              window.localStorage.setItem("token", this.token.result.token);
              console.log(window.localStorage);
            }
            
            resolve(data);
          },
          err => {
            reject(err);
          }
        );
    });
  }


  getFeedbacks() {
    return this.http.get('https://mpsp-dms.herokuapp.com/api/feedbacks');
  }

  postFeedback(feedback) {
    return this.http.post('https://mpsp-dms.herokuapp.com/api/feedbacks', feedback);

  }
}

