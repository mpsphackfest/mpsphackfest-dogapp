import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpProvider } from '../providers/http/http';
import { HttpModule } from '@angular/http';
import { IBeacon } from '@ionic-native/ibeacon';



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { FeedbackPageModule } from './../pages/feedback/feedback.module';
import { DogsScannerPageModule } from './../pages/dogs-scanner/dogs-scanner.module';
import { FeedbackMenuPageModule } from './../pages/feedback-menu/feedback-menu.module';
import { ApplicationPageModule } from './../pages/application/application.module';
import { RegisterPageModule } from './../pages/register/register.module';
import { LesenSubPageModule } from './../pages/lesen-sub/lesen-sub.module';
import { LesenRenewPageModule } from './../pages/lesen-renew/lesen-renew.module';
import { LesenStatusPageModule } from './../pages/lesen-status/lesen-status.module';
import { LoginRegisterPageModule } from './../pages/login-register/login-register.module';
import { MessagePageModule } from './../pages/message/message.module';
import { LoginPageModule } from './../pages/login/login.module';
import { ProfilePageModule } from './../pages/profile/profile.module';
import { MylicensePageModule } from './../pages/mylicense/mylicense.module';
import { PayPageModule } from './../pages/pay/pay.module';


import { FeedbackListPageModule } from './../pages/feedback-list/feedback-list.module';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    LesenSubPageModule,
    LesenRenewPageModule,
    LesenStatusPageModule,
    RegisterPageModule,
    ApplicationPageModule,
    FeedbackMenuPageModule,
    FeedbackPageModule,
    FeedbackListPageModule,
    DogsScannerPageModule,
    LoginRegisterPageModule,
    MessagePageModule,
    LoginPageModule,
    ProfilePageModule,
    MylicensePageModule,
    PayPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HttpProvider,
    Geolocation,
    IBeacon
  ]
})
export class AppModule { }
