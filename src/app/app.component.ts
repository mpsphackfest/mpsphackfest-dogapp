import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginRegisterPage } from '../pages/login-register/login-register';

// import { FCM } from '@ionic-native/fcm';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(
   // private fcm: FCM,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
    ) {
    platform.ready().then(() => {

      //Notifications
     /* fcm.subscribeToTopic('all');
      fcm.getToken().then(token=>{
          console.log(token);
      })
      fcm.onNotification().subscribe(data=>{
        if(data.wasTapped){
          console.log("Received in background");
        } else {
          console.log("Received in foreground");
        };
      })
      fcm.onTokenRefresh().subscribe(token=>{
        console.log(token);
      });*/
      //end notifications.
       statusBar.styleDefault();
       splashScreen.hide();
     });

    if (window.localStorage.getItem('token')){
      this.rootPage = HomePage
    }else{
      this.rootPage = LoginRegisterPage;
    }
  }
}

