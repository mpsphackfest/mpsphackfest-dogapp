import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import {HomePage} from '../home/home';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
	input={
		fullName:'',
		identityCard:'',
		phoneNumber:'',
		email:'',
		tax:'',
		address:'',
		city:'',
		postcode:'',
		password:'',
	}

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController,
    public httpprovider:HttpProvider
  	) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerForm(){
    let loading = this.loadingCtrl.create({
    spinner: 'ios',
    content: 'Loading Please Wait...'
  });

  loading.present()
  let data = {
    full_name:this.input.fullName,
    ic_number:this.input.identityCard,
    phonenumber:this.input.phoneNumber,
    email:this.input.email,
    tax_no:this.input.tax,
    address:this.input.address,
    city:this.input.city,
    postcode:this.input.postcode,
    group_id:"1",
    area:"1",
    premise_status:"1",
    premise_type:"1",
    dog_amount:"6"
  }

  console.log(data)

  if(data.full_name == '' || data.ic_number == '' || data.phonenumber == '' || data.email == '' || data.tax_no == '' || data.address == '' || data.city == '' || data.postcode == ''){
    let toast = this.toastCtrl.create({
      message: 'User was added successfully',
      duration: 3000,
      position: 'top'
    });
    toast.present()
  }else{
    this.httpprovider.registerUser(data).then((result) => {
       let response = result;
       loading.dismiss();

       console.log(response);
       this.navCtrl.setRoot(HomePage);

     },(err) => {
         console.log(err);
     });
  }
 }

  skipreg(){
    this.navCtrl.setRoot(HomePage);
  }
}
