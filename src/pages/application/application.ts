import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import {HomePage} from '../home/home';


/**
 * Generated class for the ApplicationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-application',
  templateUrl: 'application.html',
})
export class ApplicationPage {
	input={
		premisAddress:'Selangor',
		city:'Bangi',
		distreet:'1',
		premisStatus:'1',
		premisType:'1',
		totalDog:'1',
		dogGender:'',
		applicationDate:'Jul 15,2018',
    dog_colour:''
	}


  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController,
    public httpprovider:HttpProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApplicationPage');
  }

   registerDog(){
    let loading = this.loadingCtrl.create({
    spinner: 'ios',
    content: 'Loading Please Wait...'
  });

    console.log(this.input)

  loading.present()
  // let data={
  //       dog_address: this.input.premisAddress,
  //       dog_city: this.input.city,
  //       dog_postcode: "18000",
  //       area: "1",
  //       premise_status: this.input.premisStatus,
  //       premise_type: this.input.premisType,
  //       dog_amount: this.input.totalDog,
  //       dogs:["F","Putih"]

  //     }
      // console.log(data)


     this.httpprovider.registerDog(this.input).then((result) => {
       ;

       let response = result;
       loading.dismiss();

       console.log(response);
       if(response == 'Email already exists')
       { console.log('lalu')
         let toast = this.toastCtrl.create({
          message: 'Email already exists',
           duration: 3000,
          position: 'bottom'

  });
                   toast.present();

       }
       else{
         this.navCtrl.push(HomePage);
       }
                  
     },
         (err) => {
         console.log('lalu');
         console.log(err);
     });

 } 

}
