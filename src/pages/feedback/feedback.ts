import { HttpProvider } from './../../providers/http/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  isLocating = false;
  coords: Coordinates;
  address = '';
  district = '';
  notes = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public http: HttpProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  getCurrentLocation() {
    this.isLocating = true;

    navigator.geolocation.getCurrentPosition(
      (position) => {

        this.coords = position.coords;
        this.http.getAddress(this.coords)
          .subscribe(data => {
            const result = data.json()['results'][0];
            const poscode = result['address_components'].filter(comp => comp['types'].filter(type => type == 'postal_code').length > 0)[0]["long_name"];

            this.address = result['formatted_address'];
            this.district = poscode == 14000 ? 'Seberang Perai Tengah' : ''
            this.isLocating = false;

          });

      }, (error) => {

        this.isLocating = false;

        const toast = this.toastCtrl.create({
          message: error.message,
          duration: 3000
        });

        toast.present();

      }
    )
  }

  getCoordinate() {
    return this.coords ? this.coords.latitude + ',' + this.coords.longitude : '';
  }

  submit() {
    const feedback = {
      latitude: this.coords.latitude,
      longitude: this.coords.longitude,
      location: this.address,
      district: this.district,
      notes: this.notes
    };

    this.http.postFeedback(feedback).subscribe(
      (next) => {
        const alert = this.alertCtrl.create({
          title: 'Aduan Berjaya!',
          subTitle: 'Aduan anda tadi telah berjaya dihantar.',
          buttons: ['OK']
        });

        alert.present().then(() => this.navCtrl.popTo(this.navCtrl.getByIndex(2)));
      }, (error) => {
        const toast = this.toastCtrl.create({
          message: error.message,
          duration: 3000
        });

        toast.present();
      }
    )
  }

}
