import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DogsScannerPage } from './dogs-scanner';

@NgModule({
  declarations: [
    DogsScannerPage,
  ],
  imports: [
    IonicPageModule.forChild(DogsScannerPage),
  ],
})
export class DogsScannerPageModule {}
