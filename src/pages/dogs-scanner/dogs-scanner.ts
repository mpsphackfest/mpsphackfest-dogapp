import { IBeacon } from '@ionic-native/ibeacon';
import { FeedbackPage } from './../feedback/feedback';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DogsScannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dogs-scanner',
  templateUrl: 'dogs-scanner.html',
})
export class DogsScannerPage {

  isSearching = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ibeacon: IBeacon) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DogsScannerPage');
    this.setupScanner()
  }

  setupScanner() {
    // Request permission to use location on iOS
    this.ibeacon.requestAlwaysAuthorization();

    // create a new delegate and register it with the native layer
    let delegate = this.ibeacon.Delegate();

    // Subscribe to some of the delegate's event handlers
    delegate.didRangeBeaconsInRegion()
      .subscribe(
        data => console.log('didRangeBeaconsInRegion: ', data),
        error => console.error()
      );

    delegate.didStartMonitoringForRegion()
      .subscribe(
        data => console.log('didStartMonitoringForRegion: ', data),
        error => console.error()
      );

    delegate.didEnterRegion()
      .subscribe(
        data => {
          console.log('didEnterRegion: ', data);
        }
      );

    let beaconRegion = this.ibeacon.BeaconRegion('Estimotes', '48E47A6D-F366-A502-CD5D-1EC12DC66282')

    this.ibeacon.startMonitoringForRegion(beaconRegion)
      .then(
        () => console.log('Native layer recieved the request to monitoring'),
        error => console.error('Native layer failed to begin monitoring: ', error)
      );
  }

  searchDogsNearby() {

  }

  newFeedback() {
    this.navCtrl.push(FeedbackPage)
  }

}
