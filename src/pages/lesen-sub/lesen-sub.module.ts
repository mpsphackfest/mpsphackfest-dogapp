import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LesenSubPage } from './lesen-sub';

@NgModule({
  declarations: [
    LesenSubPage,
  ],
  imports: [
    IonicPageModule.forChild(LesenSubPage),
  ],
})
export class LesenSubPageModule {}
