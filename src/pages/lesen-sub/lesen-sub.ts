import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApplicationPage } from '../application/application';
import { LesenRenewPage } from '../lesen-renew/lesen-renew';
import { LesenStatusPage } from '../lesen-status/lesen-status';




/**
 * Generated class for the LesenSubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lesen-sub',
  templateUrl: 'lesen-sub.html',
})
export class LesenSubPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LesenSubPage');
  }

  application() {
    this.navCtrl.push(ApplicationPage);
  }

  renew() {
    this.navCtrl.push(LesenRenewPage);
  }

  status() {
    this.navCtrl.push(LesenStatusPage);
  }

}
