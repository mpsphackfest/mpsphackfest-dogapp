import { HttpProvider } from './../../providers/http/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

/**
 * Generated class for the FeedbackListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback-list',
  templateUrl: 'feedback-list.html',
})
export class FeedbackListPage {

  feedbacks;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpProvider, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackListPage');

    this.http.getFeedbacks().subscribe(
      next => {
        this.feedbacks = next.json()['data'];
      }, error => {
        const toast = this.toastCtrl.create({
          message: error.message,
          duration: 3000
        });

        toast.present();
      }
    );
  }

}
