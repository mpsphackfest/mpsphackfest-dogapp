import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import {LesenSubPage} from '../lesen-sub/lesen-sub';


/**
 * Generated class for the LesenRenewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lesen-renew',
  templateUrl: 'lesen-renew.html',
})
export class LesenRenewPage {
	input={
		premisAddress:'',
		city:'',
		distreet:'',
		premisStatus:'',
		premisType:'',
		totalDog:'',
		dogGender:'',
		applicationDate:'',
	}
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController,
    public httpprovider:HttpProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LesenRenewPage');
  }

  registerDog(){
    let loading = this.loadingCtrl.create({
    spinner: 'ios',
    content: 'Loading Please Wait...'
  });

  loading.present()
  let data={
        dog_address: this.input.premisAddress,
        dog_city: this.input.city,
        dog_postcode: "1800",
        area: "1",
        premise_status: this.input.premisStatus,
        premise_type: this.input.premisType,
        dog_amount: this.input.totalDog,
        dogs:["F","BIRU"]

      }
      console.log(data)


     this.httpprovider.registerDog(data).then((result) => {
       ;

       let response = result;
       loading.dismiss();

       console.log(response);
       if(response == 'Email already exists')
       { console.log('lalu')
         let toast = this.toastCtrl.create({
          message: 'Email already exists',
           duration: 3000,
          position: 'bottom'

  });
                   toast.present();

       }
       else{
         this.navCtrl.push(LesenSubPage);
       }
                  
     },
         (err) => {
         console.log('lalu');
         console.log(err);
     });

 } 

}
