import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LesenRenewPage } from './lesen-renew';

@NgModule({
  declarations: [
    LesenRenewPage,
  ],
  imports: [
    IonicPageModule.forChild(LesenRenewPage),
  ],
})
export class LesenRenewPageModule {}
