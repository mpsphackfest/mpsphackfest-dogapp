import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,  LoadingController, ToastController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import { HomePage } from '../home/home';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	input={
		email:'',
		password:''
	}

  res;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public httpprovider: HttpProvider,
    public loadingCtrl: LoadingController,
    private toastCntrl: ToastController
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginn(){
  	this.navCtrl.setRoot(HomePage);

  }

    loginUser() {
      let loading = this.loadingCtrl.create({
        spinner: 'ios',
        content: 'Loading Please Wait...'
      });

     let data={
       email: this.input.email,
       password: this.input.password
     }

    console.log(data)
    loading.present();

    this.httpprovider.loginUser(data).then((result) => {
      console.log(result)
      loading.dismiss();

      this.res = result
      if(this.res.response == 'error'){
        let toast = this.toastCntrl.create({
          message: 'Invalid Email or Password',
          duration:3000,
          position: 'top'
        });
        toast.present();
      }else{
        this.navCtrl.setRoot(HomePage);
      }
    },(err) => {
      loading.dismiss();
      let toast = this.toastCntrl.create({
        message: 'Invalid Email or Password',
        duration:3000,
        position: 'top'
      });
      toast.present();
      console.log(err);
    });     
  }
}
