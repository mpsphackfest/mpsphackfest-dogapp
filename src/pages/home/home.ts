import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LesenSubPage } from '../lesen-sub/lesen-sub';
import { ApplicationPage } from '../application/application';
import { FeedbackMenuPage } from './../feedback-menu/feedback-menu';
import { ProfilePage } from '../profile/profile';
import { MessagePage } from '../message/message';
import { MylicensePage } from '../mylicense/mylicense';
import { PayPage } from '../pay/pay';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController
  ) {

  }



  sub() {
    this.navCtrl.push(LesenSubPage);
}

  application() {
    this.navCtrl.push(ApplicationPage);

  }

  feedback() {
    this.navCtrl.push(FeedbackMenuPage);
  }

  message()
  {
    this.navCtrl.push(MessagePage);
  }

  profile(){
    this.navCtrl.push(ProfilePage);
  }

  myLicense(){
    this.navCtrl.push(MylicensePage);
  }

  payPage(){
    this.navCtrl.push(PayPage);
  }



}
