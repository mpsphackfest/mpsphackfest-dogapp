import { FeedbackListPage } from './../feedback-list/feedback-list';
import { DogsScannerPage } from './../dogs-scanner/dogs-scanner';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedbackMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback-menu',
  templateUrl: 'feedback-menu.html',
})
export class FeedbackMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackMenuPage');
  }

  submitNew() {
    this.navCtrl.push(DogsScannerPage);
  }

  history() {
    this.navCtrl.push(FeedbackListPage);
  }

}
