import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackMenuPage } from './feedback-menu';

@NgModule({
  declarations: [
    FeedbackMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackMenuPage),
  ],
})
export class FeedbackMenuPageModule {}
