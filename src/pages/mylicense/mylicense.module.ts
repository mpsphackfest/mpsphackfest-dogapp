import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MylicensePage } from './mylicense';

@NgModule({
  declarations: [
    MylicensePage,
  ],
  imports: [
    IonicPageModule.forChild(MylicensePage),
  ],
})
export class MylicensePageModule {}
