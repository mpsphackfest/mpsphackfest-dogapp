import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LesenStatusPage } from './lesen-status';

@NgModule({
  declarations: [
    LesenStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(LesenStatusPage),
  ],
})
export class LesenStatusPageModule {}
